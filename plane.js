class Plane {
    constructor(normal, distance, color) {
        this.normal = normal;
        this.distance = distance;
        this.color = color;
    }
}