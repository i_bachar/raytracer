class Vector3 {
    constructor() {
        if (arguments.length === 1) {
            this.x = arguments[0].x;
            this.y = arguments[0].y;
            this.z = arguments[0].z;
        } else if (arguments.length === 3) {
            this.x = arguments[0];
            this.y = arguments[1];
            this.z = arguments[2];
        } else {
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }
        
    }    

    add(other) {
        const ret = new Vector3(this);
        ret.x += other.x;
        ret.y += other.y;
        ret.z += other.z;

        return ret;
    }

    subtract(other) {
        const ret = new Vector3(this);
        ret.x -= other.x;
        ret.y -= other.y;
        ret.z -= other.z;

        return ret;
    }

    multiply(factor) {
        const ret = new Vector3(this);
        ret.x *= factor;
        ret.y *= factor;
        ret.z *= factor;

        return ret;
    }

    dotProduct(other) {
        return this.x * other.x + this.y * other.y + this.z * other.z;
    }

    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    normalized() {        
        return this.multiply(1.0 / this.length());
    }

    print() {
        console.log(`(${this.x}, ${this.y}, ${this.z})`);
    }
}