function solveQuadraticEquation(a, b, c) {
    const delta = b * b - 4 * a * c;
    if (delta < 0) {
        return [];
    } else if (delta < 0.0001) {
        return [-b / (2 * a)];
    } else {
        return [(-b + Math.sqrt(delta)) / (2 * a), (-b - Math.sqrt(delta)) / (2 * a)].sort((a, b) => a - b);
    }
}

function intersectRaySphere(rayOrigin, rayDirection, sphereCenter, sphereRadius) {
    const a = rayDirection.dotProduct(rayDirection);
    const b = 2 * rayOrigin.dotProduct(rayDirection) - 2 * rayDirection.dotProduct(sphereCenter);
    const c = rayOrigin.dotProduct(rayOrigin) - 2 * rayOrigin.dotProduct(sphereCenter) + sphereCenter.dotProduct(sphereCenter) - sphereRadius * sphereRadius;

    let potentialT = solveQuadraticEquation(a, b, c);
    potentialT = potentialT.filter(t => t >= 0);
    if (potentialT.length === 0) {
        return null;
    } else {
        const t = potentialT[0];

        const position = rayOrigin.add(rayDirection.multiply(t)); 
        return {
            position,
            normal: position.subtract(sphereCenter).normalized()
        }
    }
}

function intersectRayPlane(rayOrigin, rayDirection, planeNormal, planeDistance) {
    const denom = rayDirection.dotProduct(planeNormal);
	if (Math.abs(denom) < 0.0001)
		return null;

    const distance = (planeDistance - rayOrigin.dotProduct(planeNormal)) / denom;
    if (distance < 0) {
        return null;
    }
    const position = rayOrigin.add(rayDirection.multiply(distance)); 
    return {
        position,
        normal: new Vector3(planeNormal)
    }    
}