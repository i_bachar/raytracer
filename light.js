const POINT_LIGHT = 0;
const DIRECTIONAL_LIGHT = 1;
class Light {
    constructor(type, color, ...args) {
        this.type = type;                
        this.color = color;
        if (this.type === DIRECTIONAL_LIGHT) {
            this.direction = args[0];        
        } else if (this.type === POINT_LIGHT) {
            this.position = args[0];        
            this.range = args[1];
        }
    }
}