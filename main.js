window.onload = () => {
    const canvas = document.querySelector('#canvas');
    const ctx = canvas.getContext('2d');
    
    const imageData = ctx.createImageData(canvas.width, canvas.height);
    const data = imageData.data;
    
    const cameraFOV = 60;
    let pixelIndex = 0;

    const objects = [new Sphere(new Vector3(3, -4.0, 20), 1, new Vector3(1, 1, 1)), 
                     new Sphere(new Vector3(-3, -4.0, 20), 1, new Vector3(1, 1, 1)), 
                     new Plane(new Vector3(0, 1, 0), -4.9, new Vector3(1, 1, 1))];
    const lights = [new Light(DIRECTIONAL_LIGHT, new Vector3(0.5, 0.5, 0.5), new Vector3(1, -1, 1).normalized()), 
                    new Light(POINT_LIGHT, new Vector3(0, 1, 0), new Vector3(3, 0, 20), 8),
                    new Light(POINT_LIGHT, new Vector3(1, 0, 0), new Vector3(-3, 0, 20), 8),
                    new Light(POINT_LIGHT, new Vector3(0, 0, 1), new Vector3(0, 0, 23), 8)];
    const ambientFactor = 0.1;

    const intersectRay = (rayOrigin, rayDirection, objToIgnore=null) => {
        let closestObj = null;
        let closestDist = Infinity;
        let closestIntersection = null;
        for (let obj of objects) {
            if (obj === objToIgnore) {
                continue;
            }
            if (obj instanceof Sphere) {
                const sphereCenter = obj.center;
                const sphereRadius = obj.radius;
                const intersection = intersectRaySphere(rayOrigin, rayDirection, sphereCenter, sphereRadius);
                if (intersection) {
                    const dist = intersection.position.subtract(rayOrigin).length();
                    if (dist < closestDist) {
                        closestDist = dist;
                        closestObj = obj;
                        closestIntersection = intersection;
                    }
                }
            } else if (obj instanceof Plane) {
                const planeNormal = obj.normal;
                const planeDistance = obj.distance;
                const intersection = intersectRayPlane(rayOrigin, rayDirection, planeNormal, planeDistance);
                if (intersection) {
                    const dist = intersection.position.subtract(rayOrigin).length();
                    if (dist < closestDist) {
                        closestDist = dist;
                        closestObj = obj;
                        closestIntersection = intersection;
                    }
                }
            }
        }

        return [closestObj, closestIntersection];
    };

    for (let y = 0; y < canvas.height; ++y) {
        for (let x = 0; x < canvas.width; ++x) {
            let r = 0, g = 0 ,b = 0;
            
            const rayOrigin = new Vector3(0, 0, 0);
            const halfHeight = Math.tan((cameraFOV / 2) / 180.0 * Math.PI);
            const halfWidth = (canvas.width / canvas.height) * halfHeight;

            const pixelFactorX = x / (canvas.width - 1);
            const pixelFactorY = 1.0 - y / (canvas.height - 1);

            const px = (pixelFactorX * halfWidth * 2) - halfWidth;
            const py = (pixelFactorY * halfHeight * 2) - halfHeight;

            const posOnPlane = new Vector3(px, py, 1);            
            const rayDirection = posOnPlane.subtract(rayOrigin).normalized();

            const [closestObj, closestIntersection] = intersectRay(rayOrigin, rayDirection);

            if (closestObj) {
                r = closestObj.color.x * ambientFactor;
                g = closestObj.color.y * ambientFactor;
                b = closestObj.color.z * ambientFactor;
                for (light of lights) {
                    let lightDirection;
                    let distanceFactor = 1;
                    if (light.type === DIRECTIONAL_LIGHT) {
                        lightDirection = light.direction;
                    } else if (light.type === POINT_LIGHT) {
                        lightDirection = closestIntersection.position.subtract(light.position).normalized();
                        distanceFactor = 1.0 - closestIntersection.position.subtract(light.position).length() / light.range;
                        distanceFactor = Math.min(Math.max(distanceFactor, 0), 1);
                    }

                    let shadowFactor = 1;
                    const [shadowClosestObj, shadowClosestIntersection] = intersectRay(closestIntersection.position, lightDirection.multiply(-1), closestObj);
                    if (shadowClosestObj) {
                        if (light.type === DIRECTIONAL_LIGHT) {
                            shadowFactor = 0;
                        } else if (light.type === POINT_LIGHT) {
                            if (closestIntersection.position.subtract(shadowClosestIntersection.position).length() < closestIntersection.position.subtract(light.position).length()) {
                                shadowFactor = 0;
                            }
                        }
                    }

                    const diffuseFactor = Math.max(lightDirection.multiply(-1).dotProduct(closestIntersection.normal), 0);
                    
                    r += closestObj.color.x * diffuseFactor * light.color.x * distanceFactor * shadowFactor;
                    g += closestObj.color.y * diffuseFactor * light.color.y * distanceFactor * shadowFactor;
                    b += closestObj.color.z * diffuseFactor * light.color.z * distanceFactor * shadowFactor;
                }
            }
            
            r = Math.min(Math.max(r, 0), 1);
            g = Math.min(Math.max(g, 0), 1);
            b = Math.min(Math.max(b, 0), 1);

            data[pixelIndex + 0] = r * 255;
            data[pixelIndex + 1] = g * 255;
            data[pixelIndex + 2] = b * 255;
            data[pixelIndex + 3] = 255;
            pixelIndex += 4;
        }    
    }
    
    ctx.putImageData(imageData, 0, 0);
}